package id.ws.justjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText editNama;
    CheckBox checkCream, checkChocolate;
    TextView txtJumlah, txtTagihan;
    Button buttonMin, buttonPlus, btnOrder;

    int jumlahPesanan = 0;
    int hargaKopi = 2;
    int hargaCream = 1;
    int hargaChocolate = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editNama = findViewById(R.id.edit_nama);
        checkCream = findViewById(R.id.check_cream);
        checkChocolate = findViewById(R.id.check_chocolate);
        txtJumlah = findViewById(R.id.txt_jumlah_pesanan);
        txtTagihan = findViewById(R.id.txt_tagihan);
        buttonMin = findViewById(R.id.btn_min);
        buttonPlus = findViewById(R.id.btn_plus);
        btnOrder = findViewById(R.id.btn_order);

        txtJumlah.setText(String.valueOf(jumlahPesanan));

        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jumlahPesanan < 20){
                    tambahPesanan();
                }else {
                    Toast.makeText(getApplication(), "Pesanan maksimal 20", Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jumlahPesanan != 0) {
                    kurangPesanan();
                }else {
                    Toast.makeText(getApplicationContext(), "Pesanan tidak boleh negatif", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = namaSaya();
                hitungTagihan(nama);
            }
        });
    }

    private String namaSaya(){
        String nama = "Wisnu";
        return nama;
    }

    private void tambahPesanan(){
        jumlahPesanan = jumlahPesanan + 1;
        updateJumlahPesanan();

    }

    private void kurangPesanan(){
        jumlahPesanan = jumlahPesanan - 1;
        updateJumlahPesanan();
    }

    private void updateJumlahPesanan(){
        txtJumlah.setText(String.valueOf(jumlahPesanan));
    }

    private void hitungTagihan(String nama){
        int totalHarga = hargaKopi;

        Contoh contoh = new Contoh();
        String namaSaya = contoh.getNama();

        if(checkCream.isChecked()){
            totalHarga = totalHarga + hargaCream;
        }

        if(checkChocolate.isChecked()){
            totalHarga = totalHarga + hargaChocolate;
        }

        int totalTagihan = jumlahPesanan * totalHarga;

        String nota = nama + "\n------------------------------\nTotal tagihan: " + totalTagihan;

        txtTagihan.setText(nota);
    }
}
